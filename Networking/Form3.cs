﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Networking
{
    public partial class Form3 : Form
    {
        Form1 f;
        string type;
        public Form3(Form1 f,string type)
        {
            
            InitializeComponent();
            this.f = f;
            this.type = type;
            if (type == "telnet")
            {
                chk.Text = "Change Telnet Username";
            }
            else if (type == "ssid")
            {
                chk.Text = "Change SSID";

            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (type == "ssid")
            {


                if (textBox1.Text.Trim() != string.Empty)
                {

                    Thread t = new Thread(f.ChangePassword_Wifi);
                    if (chk.Checked && textBox2.Text != string.Empty.Trim())
                        t.Start(new object[] { textBox1.Text, textBox2.Text, f });
                    else
                        t.Start(new object[] { textBox1.Text, "", f });



                }
            }
            else
            {
                if (textBox1.Text.Trim() != string.Empty)
                {
                   
                    Thread t = new Thread(f.ChangePassword_telnet);
                    if(chk.Checked && textBox2.Text !=string.Empty.Trim())
                        t.Start(new object[] { textBox1.Text, textBox2.Text, f });
                    else
                        t.Start(new object[] { textBox1.Text, "", f });



                }
            }
            this.Close();

        }

        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            if (chk.Checked)
                textBox2.Enabled = true;
            else
                textBox2.Enabled = false;

        }
    }
}
