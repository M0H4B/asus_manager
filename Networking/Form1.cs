﻿using MinimalisticTelnet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Networking
{
    public delegate void p2f(string name, string ip, string mac, int telnet);
   
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
          

        }

        [DllImport("iphlpapi.dll", ExactSpelling = true)]
        public static extern int SendARP(int destIp, int srcIP, byte[] macAddr, ref uint physicalAddrLen);
        List<IPAddress> addresses;
        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataset.NODES' table. You can move, or remove it, as needed.
            this.nODESTableAdapter1.Fill(this.dataset.NODES);

        }


        void do__d(object o)
        {
            object[] objs = (object[])o;
            Form1 f = (Form1)objs[0];
            p2f p2 = (p2f)objs[1];
            int increment = addresses.Count / 100;
            foreach (var item in addresses)
            {
                f.Invoke((MethodInvoker)delegate
                {
                    progressBar2.Increment(increment);
                });
                try
                {

                    string ip = item.ToString();
                    Ping p = new Ping();
                    PingOptions options = new PingOptions();
                    options.DontFragment = true;
                    PingReply pp = p.Send(ip, 1, new byte[0], options);
                    if (pp.Status == IPStatus.Success)
                    {
                        string name;
                        string mac = "";
                        try
                        {
                            mac = GetMacAddress(ip);
                            IPHostEntry hostEntry = Dns.GetHostEntry(ip);
                            name = hostEntry.HostName;
                        }
                        catch
                        {
                            name = "?";
                        }
                        lock (lockObj)
                        {
                            upCount++;
                            if (CheckTelnet(ip,23))
                                f.Invoke(p2, name, ip, mac, 1);
                            else
                                f.Invoke(p2, name, ip, mac, 2);
                           


                        }
                        f.Invoke((MethodInvoker)delegate
                        {
                            lblstat.Text = upCount.ToString() + " Host Found!";
                        });
                    }
                }
                catch { }
            }
            f.Invoke((MethodInvoker)delegate
            {
                Search.Enabled = true;
                f.textBox1.Enabled = true;
                progressBar2.Increment(100);
                lblstat.ForeColor = Color.Green;
                lblstat.Text = "Done!";

            });



        }

        private string GetMacAddress(string ip)
        {
            try
            {

                IPAddress dst = IPAddress.Parse(ip); // the destination IP address

                byte[] macAddr = new byte[6];
                uint macAddrLen = (uint)macAddr.Length;

                if (SendARP(BitConverter.ToInt32(dst.GetAddressBytes(), 0), 0, macAddr, ref macAddrLen) != 0)
                    throw new InvalidOperationException("SendARP failed.");

                string[] str = new string[(int)macAddrLen];
                for (int i = 0; i < macAddrLen; i++)
                    str[i] = macAddr[i].ToString("x2");

                return string.Join(":", str);
            }
            catch { return ""; }
        }

        static int upCount = 0;
        static object lockObj = new object();

        void fillGrid(string name, string ip, string mac, int telnet)
        {
            string state = "";
             if (telnet == 1)
            {
                state = "Available";
            }
            else if (telnet == 2)
            {
                state = "Not Available";
            }
            
           int x= dataGridView2.Rows.Add(name,ip,mac,state);
        }



     
        private void Search_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                lblstat.Text = "Searching...";
                string[] octets = textBox1.Text.Split('.');
                int start = BitConverter.ToInt32(new byte[] { Convert.ToByte(octets[3]), Convert.ToByte(octets[2]), Convert.ToByte(octets[1]), Convert.ToByte(octets[0]) }, 0);
                string[] octets2 = textBox2.Text.Split('.');
                int end = BitConverter.ToInt32(new byte[] { Convert.ToByte(octets2[3]), Convert.ToByte(octets2[2]), Convert.ToByte(octets2[1]), Convert.ToByte(octets2[0]) }, 0);
                addresses = new List<IPAddress>();
                for (int i = start; i <= end; i++)
                {
                    byte[] bytes = BitConverter.GetBytes(i);
                    addresses.Add(new IPAddress(new[] { bytes[3], bytes[2], bytes[1], bytes[0] }));
                }
                progressBar2.Value = 0;
                //        MessageBox.Show(((Convert.ToDouble(100) / Convert.ToDouble(addresses.Count))).ToString());

                Thread t = new Thread(do__d);
                p2f p = new p2f(fillGrid);
                t.Start(new object[] { this, p });
                Search.Enabled = false;
                textBox1.Enabled = false;
                textBox2.Enabled = false;

            }
            else
                MessageBox.Show("Set start Ip at least !");
        }
        MatchCollection result;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox2.Text = textBox1.Text;
            Regex ip = new Regex(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b");
            result = ip.Matches(textBox1.Text);
            if (result.Count > 0)
                Search.Enabled = true;
            else
                Search.Enabled = false;

        }

        private void nODESBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.nODESBindingSource1.EndEdit();
                this.tableAdapterManager1.UpdateAll(this.dataset);
              //  this.nODESTableAdapter1.Fill(this.dataset.NODES);
                this.nODESTableAdapter1.Update(this.dataset.NODES);


            }
            catch { MessageBox.Show("Not saved !"); }

        }
       

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView2.CurrentRow != null)
            {
                button1.Enabled = true;
               
            }
            else
            { button1.Enabled = false;

                
            }
        }

        protected bool CheckTelnet(string destServerIp,int port)

        {

            TcpClient oClient = new TcpClient();

            try

            {

                oClient.Connect(destServerIp, port);
                if (oClient.Connected)

                    oClient.Close();
                return true;

            }
            catch

            {
                if (oClient.Connected)

                    oClient.Close();
                return false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView2.CurrentRow.Cells[3].Value.ToString() == "Available")
                {
                    this.nODESBindingNavigator.AddNewItem.PerformClick();
                    dEVICENAMETextBox.Text = dataGridView2.CurrentRow.Cells["Column1"].Value.ToString();
                    iPADDRESSTextBox.Text = dataGridView2.CurrentRow.Cells["Column2"].Value.ToString();
                    mACADDRESSTextBox.Text = dataGridView2.CurrentRow.Cells["Column3"].Value.ToString();
                    pORTTextBox.Text = "23";
                    lOGINNAMETextBox.Text = "";
                    pASSWORDTextBox.Text = "";
                    nOTESTextBox.Text = "";
                    tabControl1.SelectedTab = tabPage3;
                    nODESBindingNavigatorSaveItem_Click_1(sender, e);
                }
                else
                    MessageBox.Show("Can't save Device to database Telnet Not Available!!");
            }
            catch(Exception whhy) {
                MessageBox.Show(whhy.Message);
                this.nODESBindingNavigator.DeleteItem.PerformClick();
            }
        }

     

        private void toolStripConnect_Click(object sender, EventArgs e)
        {
            if (nODESDataGridView.SelectedRows.Count == 0) { MessageBox.Show("Please Select Device Row First !"); return; }
            else if (nODESDataGridView.SelectedRows.Count != 1) { MessageBox.Show("Please select just one device to connect"); return; }
            AllocConsole();
            try
            {
                Console.Title = "Telnet  "+ nODESDataGridView.CurrentRow.Cells["dataGridViewTextBoxColumn2"].Value.ToString() + ":"+ nODESDataGridView.CurrentRow.Cells[4].Value.ToString();
                TelnetConnection tc;
                tc = new TelnetConnection(nODESDataGridView.CurrentRow.Cells["dataGridViewTextBoxColumn2"].Value.ToString(), Convert.ToInt32(nODESDataGridView.CurrentRow.Cells["dataGridViewTextBoxColumn4"].Value));
                string s = tc.Login(nODESDataGridView.CurrentRow.Cells["dataGridViewTextBoxColumn5"].Value.ToString(), nODESDataGridView.CurrentRow.Cells["dataGridViewTextBoxColumn6"].Value.ToString(), 100);
                Console.Write(s);
                s = tc.Read();
                string prompt = s.TrimEnd();
                if (s.TrimEnd().Contains("#") || s.Contains("/root#") || s.Contains(":/tmp/home/root#"))
                {
                    Console.Write(prompt);
                    while (tc.IsConnected && prompt.Trim() != "exit")
                    {
                        if (prompt.Trim() == "cls" || prompt.Trim() == "clear")
                            Console.Clear();
                        else if (prompt.Trim() == "exit")
                            break;
                        prompt = Console.ReadLine();
                        tc.WriteLine(prompt);
                        Console.Write(tc.Read());

                    }
                }
            }
            catch { }
            FreeConsole();

        }
        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern int FreeConsole();
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            
            if (nODESDataGridView.Rows.Count == 0)
            {
                return;
            }
            if (nODESDataGridView.SelectedRows.Count == 0) { MessageBox.Show("Please Select Device Row First !"); return; }
            toolStripProgressBar1.Value = 0;
            Form3 f = new Form3(this,"telnet");
            f.ShowDialog();

        }

        public void ChangePassword_Wifi(object oo)
        {

            object[] objs = (object[])oo;
            string _newPassword = (string)objs[0];
            string loginname = (string)objs[1];
            Form1 f = (Form1)objs[2];
            int x = 0;
            if (_newPassword != string.Empty)
            {
                foreach (DataGridViewRow r in nODESDataGridView.SelectedRows)
                {
                    int increment = (int)((((double)r.Index == 0 ? 1 : (double)r.Index) / (double)nODESDataGridView.Rows.Count) * 100);

                    f.Invoke((MethodInvoker)delegate
                    {
                        toolStripProgressBar1.Increment(increment);

                    });
                    try
                    {
                        //check telnet  .Contains("Password:")
                        if (CheckTelnet(r.Cells["dataGridViewTextBoxColumn2"].Value.ToString(), Convert.ToInt32(r.Cells["dataGridViewTextBoxColumn4"].Value.ToString())))
                        {

                            TelnetConnection tc = new TelnetConnection(r.Cells["dataGridViewTextBoxColumn2"].Value.ToString(), Convert.ToInt32(r.Cells["dataGridViewTextBoxColumn4"].Value.ToString()));
                            string temp = tc.Login(r.Cells["dataGridViewTextBoxColumn5"].Value.ToString(), r.Cells["dataGridViewTextBoxColumn6"].Value.ToString(), 100);
                            temp = tc.Read();
                            if (temp.TrimEnd().Contains("#") || temp.Contains("/root#") || temp.Contains(":/tmp/home/root#"))
                            {
                                temp = "";

                                /*  tc.WriteLine("nvram get http_username");
                                  string s = tc.Read();
                                  s = s.Substring(s.IndexOf("e\r\n", 1) + 1);
                                  s = s.Substring(0, s.IndexOf("\r\n", 1));
                                  r.Cells["dataGridViewTextBoxColumn8"].Value = s; s = "";

                                  tc.WriteLine("nvram get http_passwd");
                                  s = tc.Read();
                                  s = s.Substring(s.IndexOf("d\r\n", 1) + 1);
                                  s = s.Substring(0, s.IndexOf("\r\n", 1));
                                  r.Cells["dataGridViewTextBoxColumn8"].Value = s; s = ""; //"nvram get http_passwd\r\n123\r\nadmin@RT-N12:/tmp/home/root# "
                                  */
                                if (loginname != string.Empty.Trim())
                                {

                                    ///change
                                    tc.WriteLine("nvram set wl0_ssid=" + loginname);
                                    temp = tc.Read();
                                    f.Invoke((MethodInvoker)delegate
                                    {
                                        r.Cells["dataGridViewTextBoxColumn7"].Value = loginname;
                                    });
                                }
                                tc.WriteLine("nvram set wl0_wpa_psk=" + _newPassword);
                                temp = tc.Read();
                                tc.WriteLine("nvram commit");
                                temp = tc.Read();
                                tc.WriteLine("nvram get wl0_wpa_psk");
                                temp = "";
                                temp = tc.Read();

                                if (temp.Contains("nvram get wl0_wpa_psk"))
                                {
                                    tc.WriteLine("reboot");
                                    f.Invoke((MethodInvoker)delegate
                                    {
                                        r.Cells["dataGridViewTextBoxColumn8"].Value = _newPassword;
                                        r.HeaderCell.Value = "Succesfuly";
                                    });
                                    x++;
                                }
                                else f.Invoke((MethodInvoker)delegate
                                {
                                    r.HeaderCell.Value = "Faild";
                                });

                                tc.WriteLine("exit");

                            }
                            else
                                f.Invoke((MethodInvoker)delegate
                                {
                                    r.HeaderCell.Value = "Faild";
                                });


                        }
                        else
                            f.Invoke((MethodInvoker)delegate
                            {
                                r.HeaderCell.Value = "Faild";
                            });

                    }
                    catch (Exception why) { MessageBox.Show(why.Message); }
                }
            }
            nODESBindingNavigatorSaveItem_Click_1(new object(), new EventArgs());
            f.Invoke((MethodInvoker)delegate
            {
                toolStripProgressBar1.Value = 100;
                Thread.Sleep(1000);
                toolStripProgressBar1.Value = 0;

            });
            if (x != 0)
                MessageBox.Show(x.ToString() + " Nodes Succesfuly Changed Wifi password");

        }
        public  void ChangePassword_telnet(object oo)
        {

            object[] objs = (object[])oo;
            string _newPassword = (string)objs[0];
            string loginname = (string)objs[1];
            Form1 f = (Form1)objs[2];
                int x = 0;
                if (_newPassword != string.Empty)
                {
                    foreach (DataGridViewRow r in nODESDataGridView.SelectedRows)
                    {
                    int increment = (int)((((double)r.Index == 0 ? 1 : (double)r.Index) / (double)nODESDataGridView.Rows.Count) * 100);

                    f.Invoke((MethodInvoker)delegate
                    {
                        toolStripProgressBar1.Increment(increment);

                    });
                    try
                        {
                            //check telnet  .Contains("Password:")
                            if (CheckTelnet(r.Cells["dataGridViewTextBoxColumn2"].Value.ToString(), Convert.ToInt32(r.Cells["dataGridViewTextBoxColumn4"].Value.ToString())))
                            {

                                TelnetConnection tc = new TelnetConnection(r.Cells["dataGridViewTextBoxColumn2"].Value.ToString(), Convert.ToInt32(r.Cells["dataGridViewTextBoxColumn4"].Value.ToString()));
                                string temp = tc.Login(r.Cells["dataGridViewTextBoxColumn5"].Value.ToString(), r.Cells["dataGridViewTextBoxColumn6"].Value.ToString(), 100);
                                temp = tc.Read();
                                if (temp.TrimEnd().Contains("#") || temp.Contains("/root#") || temp.Contains(":/tmp/home/root#"))
                                {
                                    temp = "";

                                  /*  tc.WriteLine("nvram get http_username");
                                    string s = tc.Read();
                                    s = s.Substring(s.IndexOf("e\r\n", 1) + 1);
                                    s = s.Substring(0, s.IndexOf("\r\n", 1));
                                    r.Cells["dataGridViewTextBoxColumn8"].Value = s; s = "";

                                    tc.WriteLine("nvram get http_passwd");
                                    s = tc.Read();
                                    s = s.Substring(s.IndexOf("d\r\n", 1) + 1);
                                    s = s.Substring(0, s.IndexOf("\r\n", 1));
                                    r.Cells["dataGridViewTextBoxColumn8"].Value = s; s = ""; //"nvram get http_passwd\r\n123\r\nadmin@RT-N12:/tmp/home/root# "
                                    */
                                    if (loginname != string.Empty.Trim())
                                    {
                                        tc.WriteLine("nvram set http_username=" + loginname);
                                    f.Invoke((MethodInvoker)delegate
                                    {
                                        r.Cells["dataGridViewTextBoxColumn5"].Value = loginname;
                                    });
                                    }
    
                                    tc.WriteLine("nvram set http_passwd="+ _newPassword);
                                temp= tc.Read();
                                    tc.WriteLine("nvram commit");
                                temp=tc.Read();
                                    tc.WriteLine("nvram get http_passwd");
                                temp = "";
                                temp = tc.Read();

                                if (temp.Contains("nvram get http_passwd"))
                                {
                                    tc.WriteLine("reboot");
                                    f.Invoke((MethodInvoker)delegate
                                    {
                                        r.Cells["dataGridViewTextBoxColumn6"].Value = _newPassword;
                                        r.HeaderCell.Value = "Succesfuly";
                                    });
                                    x++;
                                }
                                else f.Invoke((MethodInvoker)delegate
                                {
                                    r.HeaderCell.Value = "Faild";
                                });

                                tc.WriteLine("exit");

                            }
                            else
                                f.Invoke((MethodInvoker)delegate
                                {
                                    r.HeaderCell.Value = "Faild";
                                });


                        }
                        else
                            f.Invoke((MethodInvoker)delegate
                            {
                                r.HeaderCell.Value = "Faild";
                            });

                    }
                        catch(Exception why) { MessageBox.Show(why.Message); }
                    }
                }
                nODESBindingNavigatorSaveItem_Click_1(new object(), new EventArgs());
            f.Invoke((MethodInvoker)delegate
            {
                toolStripProgressBar1.Value = 100;
                Thread.Sleep(1000);
                toolStripProgressBar1.Value = 0;

            });
            if (x != 0)
                MessageBox.Show(x.ToString() + " Nodes Succesfuly Changed password");

        }
        static string _newPassword = string.Empty;
       
        void Check_live_hosts(object o)
        {
            object[] oo = (object[])o;
            Form1 f = (Form1)oo[0];
             
            

            foreach (DataGridViewRow row in nODESDataGridView.Rows)
            {
                int increment = (int)((((double)row.Index == 0 ? 1 : (double)row.Index) / (double)nODESDataGridView.Rows.Count) * 100);
               
                f.Invoke((MethodInvoker)delegate
                {
                    toolStripProgressBar1.Increment(increment);

                });

                    TcpClient oClient = new TcpClient();

                f.Invoke((MethodInvoker)delegate {
               try
                {
                    oClient.Connect(row.Cells["dataGridViewTextBoxColumn2"].Value.ToString(), Convert.ToInt32(row.Cells["dataGridViewTextBoxColumn4"].Value));
                    row.HeaderCell.Style.BackColor = Color.Green;
                    row.HeaderCell.Value = "Online";
                    if (oClient.Connected)
                        oClient.Close();
                }
                catch
                {
                    if (oClient.Connected)
                        oClient.Close();
                    row.HeaderCell.Style.BackColor = Color.Red;
                    row.HeaderCell.Value = "Offline";
                }
                  
                });

            }
            f.Invoke((MethodInvoker)delegate
            {
                toolStripProgressBar1.Value=100;
                Thread.Sleep(1000);
                toolStripProgressBar1.Value = 0;

            });

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            toolStripProgressBar1.Value = 0;
            Thread t = new Thread(Check_live_hosts);
            t.Start(new object[] { this });

        }
      

        private void nODESDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (nODESDataGridView.CurrentRow != null)
            {
                toolStripConnect.Enabled = true;
                toolStripButton1.Enabled = true;
                toolStripButton3.Enabled = true;
                toolStripButton2.Enabled = true;
                toolStripButton4.Enabled = true;
            }
            else
            {
                toolStripConnect.Enabled = false; toolStripButton1.Enabled = false;
                toolStripButton3.Enabled = false;
                toolStripButton2.Enabled = false;
                toolStripButton4.Enabled = false;
            }
            }
        void reboot_routers(object o)
        {
            object[] oo = (object[])o;
            Form1 f = (Form1)oo[0];
            int x = 0;
                foreach (DataGridViewRow r in nODESDataGridView.SelectedRows)
                {

                int increment = (int)((((double)r.Index == 0 ? 1 : (double)r.Index) / (double)nODESDataGridView.Rows.Count) * 100);

                f.Invoke((MethodInvoker)delegate
                {
                    toolStripProgressBar1.Increment(increment);

                });

                try
                {
                        //check telnet  .Contains("Password:")
                        if (CheckTelnet(r.Cells["dataGridViewTextBoxColumn2"].Value.ToString(),Convert.ToInt32(r.Cells["dataGridViewTextBoxColumn4"].Value.ToString())))
                        {

                            TelnetConnection tc = new TelnetConnection(r.Cells["dataGridViewTextBoxColumn2"].Value.ToString(),Convert.ToInt32( r.Cells["dataGridViewTextBoxColumn4"].Value));
                            string temp = tc.Login(r.Cells["dataGridViewTextBoxColumn5"].Value.ToString(), r.Cells["dataGridViewTextBoxColumn6"].Value.ToString(), 100);
                            temp=tc.Read();
                        if (temp.TrimEnd().Contains("#") || temp.Contains("/root#") || temp.Contains(":/tmp/home/root#"))
                        {
                            tc.WriteLine("reboot");
                            x++;
                            r.HeaderCell.Value = "Succsess";
                        }
                        else
                            r.HeaderCell.Value = "Faild";

                    }
                    else
                        r.HeaderCell.Value = "Faild";

                }
                    catch { }
                f.Invoke((MethodInvoker)delegate
                {
                    toolStripProgressBar1.Value = 100;
                    Thread.Sleep(1000);
                    toolStripProgressBar1.Value = 0;

                });
            }
                if(x!=0)
                 MessageBox.Show(x.ToString() + " Nodes Rebooting");
        }
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (nODESDataGridView.SelectedRows.Count == 0) { MessageBox.Show("Please Select Device Row First !"); return; }
            toolStripProgressBar1.Value = 0;
            Thread t = new Thread(reboot_routers);
            t.Start(new object[] { this });
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {

            if (nODESDataGridView.Rows.Count == 0)
            {
                return;
            }
            if (nODESDataGridView.SelectedRows.Count == 0) { MessageBox.Show("Please Select Device Row First !"); return; }
            toolStripProgressBar1.Value = 0;
            Form3 f = new Form3(this, "ssid");
            f.ShowDialog();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if (nODESDataGridView.Rows.Count == 0)
            {
                return;
            }
            if (nODESDataGridView.SelectedRows.Count == 0) { MessageBox.Show("Please Select Device Row First !"); return; }
            toolStripProgressBar1.Value = 0;
            Form2 f = new Form2(this);
            f.ShowDialog();
        }
       public void Cust_cmd(object o)
        {
            object[] oo = (object[])o;
            Form1 f = (Form1)oo[0];
            ListBox.ObjectCollection Commands = (ListBox.ObjectCollection)oo[1];
            int x = 0;
            foreach (DataGridViewRow r in nODESDataGridView.SelectedRows)
            {

                int increment = (int)((((double)r.Index == 0 ? 1 : (double)r.Index) / (double)nODESDataGridView.Rows.Count) * 100);

                f.Invoke((MethodInvoker)delegate
                {
                    toolStripProgressBar1.Increment(increment);

                });

                try
                {
                    //check telnet  .Contains("Password:")
                    if (CheckTelnet(r.Cells["dataGridViewTextBoxColumn2"].Value.ToString(), Convert.ToInt32(r.Cells["dataGridViewTextBoxColumn4"].Value.ToString())))
                    {

                        TelnetConnection tc = new TelnetConnection(r.Cells["dataGridViewTextBoxColumn2"].Value.ToString(), Convert.ToInt32(r.Cells["dataGridViewTextBoxColumn4"].Value));
                        string temp = tc.Login(r.Cells["dataGridViewTextBoxColumn5"].Value.ToString(), r.Cells["dataGridViewTextBoxColumn6"].Value.ToString(), 100);
                        temp = tc.Read();
                        if (temp.TrimEnd().Contains("#") || temp.Contains("/root#") || temp.Contains(":/tmp/home/root#"))
                        {
                            foreach (var item in Commands)
                            {
                                tc.WriteLine(item.ToString());
                                temp = tc.Read();
                            }
                            tc.WriteLine("exit");
                            x++;
                            f.Invoke((MethodInvoker)delegate
                            {
                                r.HeaderCell.Value = "Succsess";
                            });
                        }
                        else
                            f.Invoke((MethodInvoker)delegate
                            { r.HeaderCell.Value = "Faild"; });
                        
                    }
                    else
                        f.Invoke((MethodInvoker)delegate
                        { r.HeaderCell.Value = "Faild"; });

                }
                catch { }
                f.Invoke((MethodInvoker)delegate
                {
                    toolStripProgressBar1.Value = 100;
                    Thread.Sleep(1000);
                    toolStripProgressBar1.Value = 0;

                });
            }
            if (x != 0)
                MessageBox.Show(x.ToString() + " Nodes Succsessfully Commands Executed");
        }
    }
}
    
