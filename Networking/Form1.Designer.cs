﻿namespace Networking
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label dEVICENAMELabel;
            System.Windows.Forms.Label iPADDRESSLabel;
            System.Windows.Forms.Label mACADDRESSLabel;
            System.Windows.Forms.Label pORTLabel;
            System.Windows.Forms.Label lOGINNAMELabel;
            System.Windows.Forms.Label pASSWORDLabel;
            System.Windows.Forms.Label sSIDLabel;
            System.Windows.Forms.Label wIFI_PRESENT_PASSWORDLabel;
            System.Windows.Forms.Label wIFI_OLD_PASSWORDLabel;
            System.Windows.Forms.Label nOTESLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dEVICENAMETextBox = new System.Windows.Forms.TextBox();
            this.nODESBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataset = new Networking.Dataset();
            this.iPADDRESSTextBox = new System.Windows.Forms.TextBox();
            this.mACADDRESSTextBox = new System.Windows.Forms.TextBox();
            this.pORTTextBox = new System.Windows.Forms.TextBox();
            this.lOGINNAMETextBox = new System.Windows.Forms.TextBox();
            this.pASSWORDTextBox = new System.Windows.Forms.TextBox();
            this.sSIDTextBox = new System.Windows.Forms.TextBox();
            this.wIFI_PRESENT_PASSWORDTextBox = new System.Windows.Forms.TextBox();
            this.wIFI_OLD_PASSWORDTextBox = new System.Windows.Forms.TextBox();
            this.nOTESTextBox = new System.Windows.Forms.TextBox();
            this.nODESDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nODESBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.nODESBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripConnect = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Search = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblstat = new System.Windows.Forms.Label();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nODESTableAdapter1 = new Networking.DatasetTableAdapters.NODESTableAdapter();
            this.tableAdapterManager1 = new Networking.DatasetTableAdapters.TableAdapterManager();
            dEVICENAMELabel = new System.Windows.Forms.Label();
            iPADDRESSLabel = new System.Windows.Forms.Label();
            mACADDRESSLabel = new System.Windows.Forms.Label();
            pORTLabel = new System.Windows.Forms.Label();
            lOGINNAMELabel = new System.Windows.Forms.Label();
            pASSWORDLabel = new System.Windows.Forms.Label();
            sSIDLabel = new System.Windows.Forms.Label();
            wIFI_PRESENT_PASSWORDLabel = new System.Windows.Forms.Label();
            wIFI_OLD_PASSWORDLabel = new System.Windows.Forms.Label();
            nOTESLabel = new System.Windows.Forms.Label();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nODESBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nODESDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nODESBindingNavigator)).BeginInit();
            this.nODESBindingNavigator.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dEVICENAMELabel
            // 
            dEVICENAMELabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            dEVICENAMELabel.AutoSize = true;
            dEVICENAMELabel.Location = new System.Drawing.Point(12, 238);
            dEVICENAMELabel.Name = "dEVICENAMELabel";
            dEVICENAMELabel.Size = new System.Drawing.Size(78, 13);
            dEVICENAMELabel.TabIndex = 2;
            dEVICENAMELabel.Text = "Device Name :";
            // 
            // iPADDRESSLabel
            // 
            iPADDRESSLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            iPADDRESSLabel.AutoSize = true;
            iPADDRESSLabel.Location = new System.Drawing.Point(12, 264);
            iPADDRESSLabel.Name = "iPADDRESSLabel";
            iPADDRESSLabel.Size = new System.Drawing.Size(62, 13);
            iPADDRESSLabel.TabIndex = 4;
            iPADDRESSLabel.Text = "Ip address :";
            // 
            // mACADDRESSLabel
            // 
            mACADDRESSLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            mACADDRESSLabel.AutoSize = true;
            mACADDRESSLabel.Location = new System.Drawing.Point(12, 299);
            mACADDRESSLabel.Name = "mACADDRESSLabel";
            mACADDRESSLabel.Size = new System.Drawing.Size(75, 13);
            mACADDRESSLabel.TabIndex = 6;
            mACADDRESSLabel.Text = "Mac Address :";
            // 
            // pORTLabel
            // 
            pORTLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            pORTLabel.AutoSize = true;
            pORTLabel.Location = new System.Drawing.Point(12, 324);
            pORTLabel.Name = "pORTLabel";
            pORTLabel.Size = new System.Drawing.Size(32, 13);
            pORTLabel.TabIndex = 8;
            pORTLabel.Text = "Port :";
            // 
            // lOGINNAMELabel
            // 
            lOGINNAMELabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            lOGINNAMELabel.AutoSize = true;
            lOGINNAMELabel.Location = new System.Drawing.Point(272, 238);
            lOGINNAMELabel.Name = "lOGINNAMELabel";
            lOGINNAMELabel.Size = new System.Drawing.Size(70, 13);
            lOGINNAMELabel.TabIndex = 10;
            lOGINNAMELabel.Text = "Login Name :";
            // 
            // pASSWORDLabel
            // 
            pASSWORDLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            pASSWORDLabel.AutoSize = true;
            pASSWORDLabel.Location = new System.Drawing.Point(272, 264);
            pASSWORDLabel.Name = "pASSWORDLabel";
            pASSWORDLabel.Size = new System.Drawing.Size(59, 13);
            pASSWORDLabel.TabIndex = 12;
            pASSWORDLabel.Text = "Password :";
            // 
            // sSIDLabel
            // 
            sSIDLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            sSIDLabel.AutoSize = true;
            sSIDLabel.Location = new System.Drawing.Point(272, 299);
            sSIDLabel.Name = "sSIDLabel";
            sSIDLabel.Size = new System.Drawing.Size(35, 13);
            sSIDLabel.TabIndex = 14;
            sSIDLabel.Text = "SSID:";
            // 
            // wIFI_PRESENT_PASSWORDLabel
            // 
            wIFI_PRESENT_PASSWORDLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            wIFI_PRESENT_PASSWORDLabel.AutoSize = true;
            wIFI_PRESENT_PASSWORDLabel.Location = new System.Drawing.Point(272, 324);
            wIFI_PRESENT_PASSWORDLabel.Name = "wIFI_PRESENT_PASSWORDLabel";
            wIFI_PRESENT_PASSWORDLabel.Size = new System.Drawing.Size(80, 13);
            wIFI_PRESENT_PASSWORDLabel.TabIndex = 16;
            wIFI_PRESENT_PASSWORDLabel.Text = "Wifi Password :";
            // 
            // wIFI_OLD_PASSWORDLabel
            // 
            wIFI_OLD_PASSWORDLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            wIFI_OLD_PASSWORDLabel.AutoSize = true;
            wIFI_OLD_PASSWORDLabel.Location = new System.Drawing.Point(569, 241);
            wIFI_OLD_PASSWORDLabel.Name = "wIFI_OLD_PASSWORDLabel";
            wIFI_OLD_PASSWORDLabel.Size = new System.Drawing.Size(124, 13);
            wIFI_OLD_PASSWORDLabel.TabIndex = 18;
            wIFI_OLD_PASSWORDLabel.Text = "WIFI OLD PASSWORD:";
            wIFI_OLD_PASSWORDLabel.Visible = false;
            // 
            // nOTESLabel
            // 
            nOTESLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            nOTESLabel.AutoSize = true;
            nOTESLabel.Location = new System.Drawing.Point(532, 242);
            nOTESLabel.Name = "nOTESLabel";
            nOTESLabel.Size = new System.Drawing.Size(41, 13);
            nOTESLabel.TabIndex = 20;
            nOTESLabel.Text = "Notes :";
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.Controls.Add(dEVICENAMELabel);
            this.tabPage3.Controls.Add(this.dEVICENAMETextBox);
            this.tabPage3.Controls.Add(iPADDRESSLabel);
            this.tabPage3.Controls.Add(this.iPADDRESSTextBox);
            this.tabPage3.Controls.Add(mACADDRESSLabel);
            this.tabPage3.Controls.Add(this.mACADDRESSTextBox);
            this.tabPage3.Controls.Add(pORTLabel);
            this.tabPage3.Controls.Add(this.pORTTextBox);
            this.tabPage3.Controls.Add(lOGINNAMELabel);
            this.tabPage3.Controls.Add(this.lOGINNAMETextBox);
            this.tabPage3.Controls.Add(pASSWORDLabel);
            this.tabPage3.Controls.Add(this.pASSWORDTextBox);
            this.tabPage3.Controls.Add(sSIDLabel);
            this.tabPage3.Controls.Add(this.sSIDTextBox);
            this.tabPage3.Controls.Add(wIFI_PRESENT_PASSWORDLabel);
            this.tabPage3.Controls.Add(this.wIFI_PRESENT_PASSWORDTextBox);
            this.tabPage3.Controls.Add(wIFI_OLD_PASSWORDLabel);
            this.tabPage3.Controls.Add(this.wIFI_OLD_PASSWORDTextBox);
            this.tabPage3.Controls.Add(nOTESLabel);
            this.tabPage3.Controls.Add(this.nOTESTextBox);
            this.tabPage3.Controls.Add(this.nODESDataGridView);
            this.tabPage3.Controls.Add(this.nODESBindingNavigator);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(839, 347);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Modify Nodes";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dEVICENAMETextBox
            // 
            this.dEVICENAMETextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dEVICENAMETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nODESBindingSource1, "DEVICENAME", true));
            this.dEVICENAMETextBox.Location = new System.Drawing.Point(98, 235);
            this.dEVICENAMETextBox.Name = "dEVICENAMETextBox";
            this.dEVICENAMETextBox.Size = new System.Drawing.Size(150, 20);
            this.dEVICENAMETextBox.TabIndex = 3;
            // 
            // nODESBindingSource1
            // 
            this.nODESBindingSource1.DataMember = "NODES";
            this.nODESBindingSource1.DataSource = this.dataset;
            // 
            // dataset
            // 
            this.dataset.DataSetName = "Dataset";
            this.dataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // iPADDRESSTextBox
            // 
            this.iPADDRESSTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.iPADDRESSTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nODESBindingSource1, "IPADDRESS", true));
            this.iPADDRESSTextBox.Location = new System.Drawing.Point(98, 261);
            this.iPADDRESSTextBox.Name = "iPADDRESSTextBox";
            this.iPADDRESSTextBox.Size = new System.Drawing.Size(150, 20);
            this.iPADDRESSTextBox.TabIndex = 5;
            // 
            // mACADDRESSTextBox
            // 
            this.mACADDRESSTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mACADDRESSTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nODESBindingSource1, "MACADDRESS", true));
            this.mACADDRESSTextBox.Location = new System.Drawing.Point(98, 296);
            this.mACADDRESSTextBox.Name = "mACADDRESSTextBox";
            this.mACADDRESSTextBox.Size = new System.Drawing.Size(150, 20);
            this.mACADDRESSTextBox.TabIndex = 7;
            // 
            // pORTTextBox
            // 
            this.pORTTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pORTTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nODESBindingSource1, "PORT", true));
            this.pORTTextBox.Location = new System.Drawing.Point(98, 321);
            this.pORTTextBox.Name = "pORTTextBox";
            this.pORTTextBox.Size = new System.Drawing.Size(150, 20);
            this.pORTTextBox.TabIndex = 9;
            // 
            // lOGINNAMETextBox
            // 
            this.lOGINNAMETextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lOGINNAMETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nODESBindingSource1, "LOGINNAME", true));
            this.lOGINNAMETextBox.Location = new System.Drawing.Point(352, 235);
            this.lOGINNAMETextBox.Name = "lOGINNAMETextBox";
            this.lOGINNAMETextBox.Size = new System.Drawing.Size(150, 20);
            this.lOGINNAMETextBox.TabIndex = 11;
            // 
            // pASSWORDTextBox
            // 
            this.pASSWORDTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pASSWORDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nODESBindingSource1, "PASSWORD", true));
            this.pASSWORDTextBox.Location = new System.Drawing.Point(352, 261);
            this.pASSWORDTextBox.Name = "pASSWORDTextBox";
            this.pASSWORDTextBox.Size = new System.Drawing.Size(150, 20);
            this.pASSWORDTextBox.TabIndex = 13;
            // 
            // sSIDTextBox
            // 
            this.sSIDTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.sSIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nODESBindingSource1, "SSID", true));
            this.sSIDTextBox.Location = new System.Drawing.Point(352, 296);
            this.sSIDTextBox.Name = "sSIDTextBox";
            this.sSIDTextBox.Size = new System.Drawing.Size(150, 20);
            this.sSIDTextBox.TabIndex = 15;
            // 
            // wIFI_PRESENT_PASSWORDTextBox
            // 
            this.wIFI_PRESENT_PASSWORDTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.wIFI_PRESENT_PASSWORDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nODESBindingSource1, "WIFI_PRESENT_PASSWORD", true));
            this.wIFI_PRESENT_PASSWORDTextBox.Location = new System.Drawing.Point(352, 324);
            this.wIFI_PRESENT_PASSWORDTextBox.Name = "wIFI_PRESENT_PASSWORDTextBox";
            this.wIFI_PRESENT_PASSWORDTextBox.Size = new System.Drawing.Size(150, 20);
            this.wIFI_PRESENT_PASSWORDTextBox.TabIndex = 17;
            // 
            // wIFI_OLD_PASSWORDTextBox
            // 
            this.wIFI_OLD_PASSWORDTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.wIFI_OLD_PASSWORDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nODESBindingSource1, "WIFI_OLD_PASSWORD", true));
            this.wIFI_OLD_PASSWORDTextBox.Location = new System.Drawing.Point(685, 235);
            this.wIFI_OLD_PASSWORDTextBox.Name = "wIFI_OLD_PASSWORDTextBox";
            this.wIFI_OLD_PASSWORDTextBox.Size = new System.Drawing.Size(47, 20);
            this.wIFI_OLD_PASSWORDTextBox.TabIndex = 19;
            this.wIFI_OLD_PASSWORDTextBox.Visible = false;
            // 
            // nOTESTextBox
            // 
            this.nOTESTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nOTESTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.nODESBindingSource1, "NOTES", true));
            this.nOTESTextBox.Location = new System.Drawing.Point(585, 239);
            this.nOTESTextBox.Multiline = true;
            this.nOTESTextBox.Name = "nOTESTextBox";
            this.nOTESTextBox.Size = new System.Drawing.Size(206, 105);
            this.nOTESTextBox.TabIndex = 21;
            // 
            // nODESDataGridView
            // 
            this.nODESDataGridView.AllowUserToAddRows = false;
            this.nODESDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nODESDataGridView.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.nODESDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.nODESDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.nODESDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            this.nODESDataGridView.DataSource = this.nODESBindingSource1;
            this.nODESDataGridView.Location = new System.Drawing.Point(5, 34);
            this.nODESDataGridView.Name = "nODESDataGridView";
            this.nODESDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.nODESDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.nODESDataGridView.RowHeadersWidth = 65;
            this.nODESDataGridView.Size = new System.Drawing.Size(831, 195);
            this.nODESDataGridView.TabIndex = 1;
            this.nODESDataGridView.SelectionChanged += new System.EventHandler(this.nODESDataGridView_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DEVICENAME";
            this.dataGridViewTextBoxColumn1.HeaderText = "DEVICENAME";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "IPADDRESS";
            this.dataGridViewTextBoxColumn2.HeaderText = "IPADDRESS";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "MACADDRESS";
            this.dataGridViewTextBoxColumn3.HeaderText = "MACADDRESS";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PORT";
            this.dataGridViewTextBoxColumn4.HeaderText = "PORT";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 30;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 45;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "LOGINNAME";
            this.dataGridViewTextBoxColumn5.HeaderText = "LOGINNAME";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "PASSWORD";
            this.dataGridViewTextBoxColumn6.HeaderText = "PASSWORD";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "SSID";
            this.dataGridViewTextBoxColumn7.HeaderText = "SSID";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "WIFI_PRESENT_PASSWORD";
            this.dataGridViewTextBoxColumn8.HeaderText = "WIFI_PRESENT_PASSWORD";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "WIFI_OLD_PASSWORD";
            this.dataGridViewTextBoxColumn9.HeaderText = "WIFI_OLD_PASSWORD";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "NOTES";
            this.dataGridViewTextBoxColumn10.HeaderText = "NOTES";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // nODESBindingNavigator
            // 
            this.nODESBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.nODESBindingNavigator.BindingSource = this.nODESBindingSource1;
            this.nODESBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.nODESBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.nODESBindingNavigator.Dock = System.Windows.Forms.DockStyle.None;
            this.nODESBindingNavigator.ImageScalingSize = new System.Drawing.Size(22, 22);
            this.nODESBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.nODESBindingNavigatorSaveItem,
            this.toolStripSeparator1,
            this.toolStripProgressBar1,
            this.toolStripButton1,
            this.toolStripButton3,
            this.toolStripConnect,
            this.toolStripButton2,
            this.toolStripButton4,
            this.toolStripButton5});
            this.nODESBindingNavigator.Location = new System.Drawing.Point(3, 3);
            this.nODESBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.nODESBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.nODESBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.nODESBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.nODESBindingNavigator.Name = "nODESBindingNavigator";
            this.nODESBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.nODESBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.nODESBindingNavigator.Size = new System.Drawing.Size(802, 29);
            this.nODESBindingNavigator.Stretch = true;
            this.nODESBindingNavigator.TabIndex = 1;
            this.nODESBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(26, 26);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 26);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(26, 26);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(26, 26);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(26, 26);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 29);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 29);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(26, 26);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(26, 26);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 29);
            // 
            // nODESBindingNavigatorSaveItem
            // 
            this.nODESBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.nODESBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("nODESBindingNavigatorSaveItem.Image")));
            this.nODESBindingNavigatorSaveItem.Name = "nODESBindingNavigatorSaveItem";
            this.nODESBindingNavigatorSaveItem.Size = new System.Drawing.Size(26, 26);
            this.nODESBindingNavigatorSaveItem.Text = "Save Data";
            this.nODESBindingNavigatorSaveItem.Click += new System.EventHandler(this.nODESBindingNavigatorSaveItem_Click_1);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 29);
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 26);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(26, 26);
            this.toolStripButton1.Text = "Check all live Nodes";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(26, 26);
            this.toolStripButton3.Text = "Reboot selected Nodes";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripConnect
            // 
            this.toolStripConnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripConnect.Image = ((System.Drawing.Image)(resources.GetObject("toolStripConnect.Image")));
            this.toolStripConnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripConnect.Name = "toolStripConnect";
            this.toolStripConnect.Size = new System.Drawing.Size(26, 26);
            this.toolStripConnect.Text = "Connect";
            this.toolStripConnect.Click += new System.EventHandler(this.toolStripConnect_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(152, 26);
            this.toolStripButton2.Text = "Change Telnet Password";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(140, 26);
            this.toolStripButton4.Text = "Change Wifi Password";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(26, 26);
            this.toolStripButton5.Text = "toolStripButton5";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(847, 373);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.lblstat);
            this.tabPage1.Controls.Add(this.progressBar2);
            this.tabPage1.Controls.Add(this.dataGridView2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(839, 347);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Scan Nodes";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(36, 289);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(266, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Save Device To Database If Telnet Available";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Search);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(15, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(827, 64);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Option";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(507, 23);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(181, 20);
            this.textBox2.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(469, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "End :";
            // 
            // Search
            // 
            this.Search.Location = new System.Drawing.Point(21, 19);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(100, 31);
            this.Search.TabIndex = 0;
            this.Search.Text = "Search";
            this.Search.UseVisualStyleBackColor = true;
            this.Search.Click += new System.EventHandler(this.Search_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(207, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Start :";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(248, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(181, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblstat
            // 
            this.lblstat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblstat.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblstat.Location = new System.Drawing.Point(744, 318);
            this.lblstat.Name = "lblstat";
            this.lblstat.Size = new System.Drawing.Size(90, 23);
            this.lblstat.TabIndex = 6;
            // 
            // progressBar2
            // 
            this.progressBar2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBar2.Location = new System.Drawing.Point(16, 318);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(626, 23);
            this.progressBar2.TabIndex = 5;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dataGridView2.Location = new System.Drawing.Point(16, 84);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(826, 200);
            this.dataGridView2.TabIndex = 2;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            this.dataGridView2.SelectionChanged += new System.EventHandler(this.dataGridView2_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Device Name ";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "IP Address";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "MAC Address";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Check Telnet";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // nODESTableAdapter1
            // 
            this.nODESTableAdapter1.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.NODESTableAdapter = this.nODESTableAdapter1;
            this.tableAdapterManager1.UpdateOrder = Networking.DatasetTableAdapters.TableAdapterManager.UpdateOrderOption.UpdateInsertDelete;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 373);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(855, 400);
            this.Name = "Form1";
            this.Text = "ASUS MANAGER";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nODESBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nODESDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nODESBindingNavigator)).EndInit();
            this.nODESBindingNavigator.ResumeLayout(false);
            this.nODESBindingNavigator.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.BindingNavigator nODESBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton nODESBindingNavigatorSaveItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripButton toolStripConnect;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Search;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblstat;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private Dataset dataset;
        private System.Windows.Forms.BindingSource nODESBindingSource1;
        private DatasetTableAdapters.NODESTableAdapter nODESTableAdapter1;
        private DatasetTableAdapters.TableAdapterManager tableAdapterManager1;
        private System.Windows.Forms.TextBox dEVICENAMETextBox;
        private System.Windows.Forms.TextBox iPADDRESSTextBox;
        private System.Windows.Forms.TextBox mACADDRESSTextBox;
        private System.Windows.Forms.TextBox pORTTextBox;
        private System.Windows.Forms.TextBox lOGINNAMETextBox;
        private System.Windows.Forms.TextBox pASSWORDTextBox;
        private System.Windows.Forms.TextBox sSIDTextBox;
        private System.Windows.Forms.TextBox wIFI_PRESENT_PASSWORDTextBox;
        private System.Windows.Forms.TextBox wIFI_OLD_PASSWORDTextBox;
        private System.Windows.Forms.TextBox nOTESTextBox;
        private System.Windows.Forms.DataGridView nODESDataGridView;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
    }
}

